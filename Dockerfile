ARG CI_PROJECT_DIR

FROM quay.io/jupyter/r-notebook:hub-5.1.0

USER root

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootG2.pem /usr/local/share/ca-certificates/DigiCertGlobalRootG2.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalG2TLSRSASHA2562020CA1.pem /usr/local/share/ca-certificates/DigiCertGlobalG2TLSRSASHA2562020CA1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

RUN apt-get update && apt-get install -y \
  cmake \
  gfortran \
  python3-pip \
#  r-cran-rglpk \
  libarmadillo-dev \
  libudunits2-dev \
  libmagick++-dev \
  libglpk-dev \
  libssl-dev \
  libgdal-dev \
  libgeos-dev \
  libproj-dev \
  zip less vim man gpg povray xvfb && \
  apt-get update && \
  apt-get install -y owncloud-client && \
  apt-get clean

# R libs
#
# Replace gcc used for R
RUN sed -i 's/x86_64-conda-linux-gnu-cc/gcc/g' /opt/conda/lib/R/etc/Makeconf && sed -i 's/x86_64-conda-linux-gnu-c++/c++/g' /opt/conda/lib/R/etc/Makeconf 

RUN Rscript -e "install.packages(pkgs=c( \
   'ggplot2', \
   'lubridate', \
   'openxlsx', \
   'terra', \
   'tibble', \
   'tidyverse' ), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)"

RUN mamba install nbgitpuller jupyterlab-git

RUN jupyter labextension disable "@jupyterlab/apputils-extension:announcements"

USER 1000
